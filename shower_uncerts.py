#!/bin/env python

from __future__ import print_function

import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True
r.gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

import sys, os
import argparse
from math import sqrt

nom_filename = "/Users/dantrim/workarea/physics_analysis/wwbb/hh_wwbb2L_signal_uncerts/samples/CENTRAL_346218_ade_mar26.root"
alt_filename = "/Users/dantrim/workarea/physics_analysis/wwbb/hh_wwbb2L_signal_uncerts/samples/CENTRAL_450573_ade_mar26.root"

class RatioCanvas :
    def __init__(self, name) :
        self.name = "c_" + name
        self.canvas = r.TCanvas(self.name,self.name, 768, 768)
        self.upper_pad = r.TPad("upper", "upper", 0.0, 0.0, 1.0, 1.0)
        self.lower_pad = r.TPad("lower", "lower", 0.0, 0.0, 1.0, 1.0)
        self.set_pad_dimensions()
    def set_pad_dimensions(self) :
        can = self.canvas
        up  = self.upper_pad
        dn  = self.lower_pad

        can.cd()
        up_height = 0.75
        dn_height = 0.30
        up.SetPad(0.0, 1.0-up_height, 1.0, 1.0)
        dn.SetPad(0.0, 0.0, 1.0, dn_height)

        up.SetTickx(0)
        dn.SetGrid(0)
        dn.SetTicky(0)

        up.SetFrameFillColor(0)
        up.SetFillColor(0)

        # set right margins
        up.SetRightMargin(0.05)
        dn.SetRightMargin(0.05)

        # set left margins
        up.SetLeftMargin(0.14)
        dn.SetLeftMargin(0.14)

        # set top margins
        up.SetTopMargin(0.7 * up.GetTopMargin())
        
        # set bottom margins
        up.SetBottomMargin(0.09)
        dn.SetBottomMargin(0.4)

        up.Draw()
        dn.Draw()
        can.Update()

        self.canvas = can
        self.upper_pad = up
        self.lower_pad = dn

def get_yields(filename, args) :

    fshort = filename.strip().split("/")[-1].replace(".root","")

    rfile = r.TFile.Open(filename)
    tree = rfile.Get("superNt")

    weight_str = "eventweightNoPRW_multi * 140.5"
    cut_str = "nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh>5.55 && isDF==1"
    #cut_str = "nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh>5.45 && isSF==1"

    h = r.TH1F("h_%s" % fshort, "", 4, 0, 4)
    cmd = "isMC>>%s" % h.GetName()
    tcut_total = "(%s) * %s" % (cut_str, weight_str)
    tree.Draw(cmd, tcut_total, "goff")

    err = r.Double(0.0)
    integral = h.IntegralAndError(0,-1, err)

    return integral, err

def calc_uncerts(args) :

    files = [nom_filename, alt_filename]
    names = ["Nom", "Alt"]
    yields = []
    errors = []

    for ifile, f in enumerate(files) :
        integral, error = get_yields(f, args)
        yields.append(integral)
        errors.append(error)
        print("%s: %.3f +/- %.3f" % (names[ifile], integral, error))

    rel_error = abs(yields[0] - yields[1]) / yields[0]
    rel_error_uncert = rel_error * sqrt( (errors[0] / yields[0]) ** 2 + (errors[1] / yields[1]) ** 2 )
    print("Relative error: %.3f +/- %.3f" % (rel_error, rel_error_uncert))

def variables_dict() :

    v = {}
    v["NN_d_hh"] = [[10, 0, 10], "d_{hh}"]
    #v["NN_d_hh"] = [[20, -6, 10], "d_{hh}"]
    #v["mbb"] = [[10, 110, 140], "m_{bb} [GeV]"]
    #v["mbb"] = [[20, 20, 160], "m_{bb} [GeV]"]
    v["mbb"] = [[20, 40, 160], "m_{bb} [GeV]"]
    v["HT2Ratio"] = [[10, 0, 1], "H_{T2}^{R}"]
    v["dRll"] = [[10, 0, 4], "#Delta R_{ll}"]
    v["mt2_bb"] = [[10, 10, 150], "m_{t2}^{bb} [GeV]"]
    return v

def make_plots(args) :

    var_to_plot = "mbb"
    var_bounds = variables_dict()[var_to_plot]
    x_bounds = var_bounds[0]
    x_label = var_bounds[1]
    n_bins = x_bounds[0]
    x_lo = x_bounds[1]
    x_hi = x_bounds[2]

    sample_files = [nom_filename, alt_filename]
    sample_names = ["Herwig7", "Pythia8"]
    colors = [r.kBlack, r.kRed+2]

    histos = []
    histos_ratio = []

    weight_str = "eventweightNoPRW_multi * 140.5"
    cut_str = "nBJets>=2 && mll>20 && mll<60"# && mbb>110 && mbb<140"
    full_cut_str = "(%s) * %s" % (cut_str, weight_str)

    sample_rfiles = [r.TFile.Open(x) for x in sample_files]
    maxys = []

    for isf, sf in enumerate(sample_rfiles) :
        tree = sf.Get("superNt")

        h = r.TH1F("h_%s_%d" % (var_to_plot, isf), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
        h.Sumw2()
        hr = r.TH1F("h_%s_%d_ratio" % (var_to_plot, isf), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
        hr.Sumw2()
        cmd = "%s>>%s" % (var_to_plot, h.GetName())
        tree.Draw(cmd, full_cut_str, "goff")

        h.GetXaxis().SetLabelOffset(-999)
        h.GetXaxis().SetTitleOffset(-999)
        h.GetYaxis().SetTitleOffset(1.7 * h.GetYaxis().GetTitleOffset())
        h.SetLineWidth(2)
        h.SetLineColor(colors[isf])

        maxys.append(h.GetMaximum())

        h.SetMinimum(0)
        histos.append(h)
        cmd = "%s>>%s" % (var_to_plot, hr.GetName())
        tree.Draw(cmd, full_cut_str, "goff")
        histos_ratio.append(hr)

    maxy = max(maxys)
    print("maxy = {}".format(maxy))
    maxy = 1.15 * maxy
    for h in histos :
        h.SetMaximum(maxy)

    rc = RatioCanvas(name = "rc_%s" % var_to_plot)
    rc.canvas.cd()
    upper = rc.upper_pad
    lower = rc.lower_pad
    upper.SetTicks(1,1)
    upper.SetGrid(1,1)
    lower.SetTicks(1,1)
    lower.SetGrid(1,1)

    # upper pad
    upper.cd()
    histos[0].Draw("hist e")
    upper.Update()
    histos[1].Draw("hist e same")
    upper.Update()

    leg = r.TLegend(0.75,0.75, 0.91, 0.91)
    leg.AddEntry(histos[0], "Herwig7", "l")
    leg.AddEntry(histos[1], "Pythia8", "l")
    leg.Draw()
    upper.Update()

    # lower pad
    lower.cd()
    den_histo = histos_ratio[0]
    ratio_histo = histos_ratio[1]
    ratio_histo.Divide(den_histo)
    ratio_histo.SetLineColor(r.kRed+2)
    ratio_histo.GetYaxis().SetNdivisions(5)
    ratio_histo.GetYaxis().SetTitle("Pythia8 / Herwig7")
    ratio_histo.SetLineWidth(2)
    ratio_histo.SetMaximum(2)
    ratio_histo.SetMinimum(0)

    x = ratio_histo.GetXaxis()
    y = ratio_histo.GetYaxis()

    x.SetLabelSize(2.5 * x.GetLabelSize())
    x.SetTitleSize(3 * x.GetTitleSize())

    y.SetLabelSize(2.5 * y.GetLabelSize())
    y.SetTitleSize(2.5 * y.GetTitleSize())
    y.SetTitleOffset(0.65 * y.GetTitleOffset())

    ratio_histo.Draw("hist e")
    lower.Update()

    line = r.TLine(x_lo, 1.0, x_hi, 1.0)
    line.SetLineWidth(2)
    line.SetLineStyle(2)
    line.SetLineColor(r.kRed)
    line.Draw()
    lower.Update()
    

    # save
    outname = "uncert_plots/hh_shower_uncert_%s.pdf" % var_to_plot
    print("Saving plot: %s" % os.path.abspath(outname))
    rc.canvas.SaveAs(outname)
  

    

    

def main() :

    parser = argparse.ArgumentParser(description = "Make shower uncert plots")
    parser.add_argument("--plots", action = "store_true", default = False,
        help = "Make plots"
    )
    args = parser.parse_args()

    if not os.path.isfile(nom_filename) :
        print("ERROR could not find nominal file (expect: {})".format(nom_filename))
        sys.exit()
    if not os.path.isfile(alt_filename) :
        print("ERROR could not find alt file (expect: {})".format(alt_filename))
        sys.exit()

    calc_uncerts(args)
    if args.plots :
        make_plots(args)

    

if __name__ == "__main__" :
    main()
