#!/bin/env python

from __future__ import print_function

import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True
r.gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

import argparse
import sys, os
import glob
from math import sqrt

sherpa_dir = "/data/uclhc/uci/user/dantrim/WWbb/sherpa_zjets_hf/samples/"
mg5_dir = "/data/uclhc/uci/user/dantrim/WWbb/mg5_zjets_hf/samples/"
xsec_file = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt"

mg5_file = "/data/uclhc/uci/user/dantrim/WWbb/mg5_zjets_hf/samples/mg5_zjets_noDY.TruthNtup_v00.root"
sherpa_file = "/data/uclhc/uci/user/dantrim/WWbb/sherpa_zjets_hf/samples/sherpa_zjets_noDY.TruthNtup_v01.root"

#r.TH1F.__init__._creates = False
#r.TH1.__init__._creates = False

class RatioCanvas :
    def __init__(self, name) :
        self.name = "c_" + name
        self.canvas = r.TCanvas(self.name,self.name, 768, 768)
        self.upper_pad = r.TPad("upper", "upper", 0.0, 0.0, 1.0, 1.0)
        self.lower_pad = r.TPad("lower", "lower", 0.0, 0.0, 1.0, 1.0)
        self.set_pad_dimensions()
    def set_pad_dimensions(self) :
        can = self.canvas
        up  = self.upper_pad
        dn  = self.lower_pad

        can.cd()
        up_height = 0.75
        dn_height = 0.30
        up.SetPad(0.0, 1.0-up_height, 1.0, 1.0)
        dn.SetPad(0.0, 0.0, 1.0, dn_height)

        up.SetTickx(0)
        dn.SetGrid(0)
        dn.SetTicky(0)

        up.SetFrameFillColor(0)
        up.SetFillColor(0)

        # set right margins
        up.SetRightMargin(0.05)
        dn.SetRightMargin(0.05)

        # set left margins
        up.SetLeftMargin(0.14)
        dn.SetLeftMargin(0.14)

        # set top margins
        up.SetTopMargin(0.7 * up.GetTopMargin())

        # set bottom margins
        up.SetBottomMargin(0.09)
        dn.SetBottomMargin(0.4)

        up.Draw()
        dn.Draw()
        can.Update()

        self.canvas = can
        self.upper_pad = up
        self.lower_pad = dn

def dsid_from_sample(sample_name) :

    find = "_13TeV."
    dsid = sample_name.strip().split(find)[1].split(".")[0]
    return dsid

def gen_xsec_list(args) :

    dirnames = [sherpa_dir, mg5_dir]
    names = ["sherpa", "mg5"]

    for iname, name in enumerate(names) :
        with open("xsec_file_%s.txt" % name, "w") as ofile :
            filelist = "list_%s.txt" % name
            with open(filelist, "r") as infile :
                for sample_file in infile :
                    sample_file = sample_file.strip()
                    if sample_file.startswith("#") : continue
                    dsid = dsid_from_sample(sample_file)
                    with open(xsec_file, "r") as xsecfile :
                        for xline in xsecfile :
                            xline = xline.strip()
                            if "dataset_number" in xline : continue
                            fields = xline.split()
                            if fields[0] != dsid : continue
                            xsec = float(fields[2])
                            eff = float(fields[3])
                            kfac = float(fields[4])
                            final_xsec = xsec * eff * kfac
                            ofile.write("%s %.2f\n" % (dsid, final_xsec))
                
#    for idir, d in enumerate(dirnames) :
#        samples = glob.glob("%s/group*.root" % d)
#        print("Found %d samples for %s" % (len(samples), names[idir]))
#
#
#            for isample, sample in enumerate(samples) :
#                dsid = dsid_from_sample(sample)
#    
#                with open(xsec_file, "r") as ifile :
#                    for line in ifile :
#                        line = line.strip()
#                        if "dataset_number" in line : continue
#                        fields = line.split()
#                        if fields[0] != dsid : continue
#                        xsec = float(fields[2])
#                        eff = float(fields[3])
#                        kfac = float(fields[4])
#                        final_xsec = xsec * eff * kfac
#                        ofile.write("%s %.2f\n" % (dsid, final_xsec))

class Sample :
    def __init__(self, name = "", filelist = "", color = "") :

        self.name = name
        self.color = color
        self.filelist = filelist
        self.files = self.load_files()
        print("%s loaded sample with %d files" % (self.name, len(self.files)))
        self.dsid_map = {}
        self.xsec_map = {}

    def get_sumw(self) :
        total_sumw = 0.0
        for f in self.filelist :
            rfile = r.TFile.Open(f)
            h = rfile.Get("h_sumw")
            total_sumw += h.GetBinContent(1)
        return total_sumw

    def load_files(self) :
        list_out = []
        with open(self.filelist) as infile :
            for line in infile :
                line = line.strip()
                if line.startswith("#") : continue
                list_out.append(line)
        return list_out
                

    def load_xsec(self, xsec_filename = "") :
        self.dsid_map = {}
        self.xsec_map = {}
        with open(xsec_filename, "r") as infile :
            for iline, line in enumerate(infile) :
                line = line.strip()
                fields = line.split()
                dsid = fields[0]
                xsec = float(fields[1])
                self.dsid_map[iline] = dsid
                self.xsec_map[dsid] = xsec

def get_selection_yields(selection_str, sample, args) :

    filelist = sample.files
    n_files = len(filelist)

    total_integral = 0.0
    total_err = 0.0

    print("get_selection_yields : %s" % sample.name)
    for ifile, filename in enumerate(filelist) :
        if ifile % 6 == 0 :
            print(" > [%02d/%02d]" % (ifile+1,  n_files))
        rfile = r.TFile.Open(filename)

        h = r.TH1F("h_%s_%d" % (sample.name, ifile), "", 100, 0, -1)

        h_sumw = rfile.Get("h_sumw")
        sumw = h_sumw.GetBinContent(1)
        current_dsid = sample.dsid_map[ifile]
        xsec = sample.xsec_map[current_dsid]

        #print(" > [%02d/%02d] %s (dsid: %s   sumw: %.2f   xsec: %.2f)" % (ifile+1, n_files, filename.split("/")[-1], current_dsid, sumw, xsec))

        weight_str = "w * %.2f * 1000. / %.2f" % (float(xsec), float(sumw))
        #weight_str = "w / %.2f" % (float(sumw))
        full_cut_str = "(%s) * %s" % (selection_str, weight_str)

        tree = rfile.Get("truth")
        #print("tree entries: %d" % tree.GetEntries())
        tree.Draw("NN_d_hh>>%s" % h.GetName(), full_cut_str, "goff")

        err = r.Double(0.0)
        integral = h.IntegralAndError(0, -1, err)
        total_integral += integral
        total_err += err * err

    total_err = sqrt(total_err)

    print("%s : %.3f +/- %.3f (rel. error [%%]: %.3f) (raw counts: %d)" % (sample.name, total_integral, total_err, 100. * total_err / total_integral, h.GetEntries()))

    return [total_integral, total_err]

def calculate_tf_uncertainty(nominal_name = "", alt_name = "", tf_dict = {}) :

    # extract sr numbers
    sr_dict = tf_dict["sr"]

    nom_sr_yields = sr_dict[nominal_name]
    sr_yield_nom = nom_sr_yields[0]
    sr_yield_err_nom = nom_sr_yields[1]

    alt_sr_yields = sr_dict[alt_name]
    sr_yield_alt = alt_sr_yields[0]
    sr_yield_err_alt = alt_sr_yields[1]

    # extract the cr numbers
    cr_dict = tf_dict["cr"]

    nom_cr_yields = cr_dict[nominal_name]
    cr_yield_nom = nom_cr_yields[0]
    cr_yield_err_nom = nom_cr_yields[1]

    alt_cr_yields = cr_dict[alt_name]
    cr_yield_alt = alt_cr_yields[0]
    cr_yield_err_alt = alt_cr_yields[1]

    # nominal transfer factor
    tf_nom = float(sr_yield_nom) / float(cr_yield_nom)
    tf_err_nom = tf_nom * sqrt( (sr_yield_err_nom / sr_yield_nom) ** 2 + (cr_yield_err_nom / cr_yield_nom) ** 2)

    # alt transfer factor
    tf_alt = float(sr_yield_alt) / float(cr_yield_alt)
    tf_err_alt = tf_alt * sqrt( (sr_yield_err_alt / sr_yield_alt) ** 2 + (cr_yield_err_alt / cr_yield_alt) ** 2)

    # now compute the relative difference on the transfer factors
    tf_uncert_numerator = abs( tf_alt - tf_nom)
    tf_uncert_numerator_err = sqrt( tf_err_alt ** 2 ) # + tf_err_nom ** 2 )

    tf_uncert = tf_uncert_numerator / tf_nom
    tf_uncert_err = tf_uncert * sqrt( (tf_uncert_numerator_err / tf_uncert_numerator) ** 2 ) # + (tf_err_nom / tf_nom) ** 2 )

    print("TF uncertainty: %.3f +/- %.3f" % (tf_uncert, tf_uncert_err))


def calc_uncerts(samples, args) :

    selections = {}
    selections["sr"] = "nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh>5"
    selections["cr"] = "nBJets>=2 && mll>70 && mll<115 && mbb>110 && mbb<140 && NN_d_hh>0"

    tf_numbers = {}

    for sel_name in selections :

        tf_numbers[sel_name] = {}

        sample_yields = []
        cut_str = selections[sel_name]
        print(60 * "=")
        print(" > %s : %s" % (sel_name, cut_str))
        for isample, sample in enumerate(samples) :
            tf_numbers[sel_name][sample.name] = []
            yields = get_selection_yields(cut_str, sample, args)
            #yields = get_selection_yields(selection_str, sample, args)
            tf_numbers[sel_name][sample.name] = [yields[0], yields[1]]
            sample_yields.append(yields)
    
        nom_yields = sample_yields[0]
        alt_yields = sample_yields[1]
    
        rel_diff = abs(nom_yields[0] - alt_yields[0]) / nom_yields[0]
        print("Relative difference:  %.3f" % rel_diff)

    calculate_tf_uncertainty(nominal_name = "sherpa", alt_name = "mg5", tf_dict = tf_numbers)

def variables_dict() :

    v = {}
    v["NN_d_hh"] = [[12, 0, 12], "d_{hh}"]
    #v["NN_d_hh"] = [[20, -6, 10], "d_{hh}"]
    #v["mbb"] = [[10, 110, 140], "m_{bb} [GeV]"]
    #v["mbb"] = [[20, 20, 160], "m_{bb} [GeV]"]
    #v["mbb"] = [[20, 40, 160], "m_{bb} [GeV]"]
    #v["HT2Ratio"] = [[10, 0, 1], "H_{T2}^{R}"]
    #v["dRll"] = [[10, 0, 4], "#Delta R_{ll}"]
    #v["mt2_bb"] = [[10, 10, 150], "m_{t2}^{bb} [GeV]"]
    return v

def make_plot(varname, samples, args) :

    plot_selection = "nBJets>=2 && mll>20 && mll<60 && mbb>110 && mbb<140 && NN_d_hh>5"

    var_bounds= variables_dict()[varname]
    x_bounds = var_bounds[0]
    x_label = var_bounds[1]
    n_bins = x_bounds[0]
    x_lo = x_bounds[1]
    x_hi = x_bounds[2]

    histos = []
    histos_ratio = []

    sample_histos = []
    sample_ratio_histos = []
    for isample, sample in enumerate(samples) :
        h_sample = r.TH1F("h_%s_%s" % (sample.name, varname), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
        h_sample_ratio = r.TH1F("hr_%s_%s" % (sample.name, varname), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
        sample_histos.append(h_sample)
        sample_ratio_histos.append(h_sample_ratio)

    for isample, sample in enumerate(samples) :

        #if isample < 1 : continue

        #h_sample = r.TH1F("h_%s_%s" % (sample.name, varname), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
        #h_sample_ratio = r.TH1F("hr_%s_%s" % (sample.name, varname), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)

        histos_sample = []
        histos_ratio_sample = []

        filelist = sample.files
        n_files = len(filelist)

        for ifile, filename in enumerate(filelist) :

            if ifile % 6 == 0 :
                print("%s : [%02d/%02d]" % (sample.name, ifile+1, n_files))

            rfile = r.TFile.Open(filename)
            tree = rfile.Get("truth")
            h_sumw = rfile.Get("h_sumw")
            sumw = h_sumw.GetBinContent(1)
            current_dsid = sample.dsid_map[ifile]
            xsec = sample.xsec_map[current_dsid]

            h = r.TH1F("h_%s_%s_%s" % (sample.name, ifile, varname), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
            h.GetXaxis().SetLabelOffset(-999)
            h.GetXaxis().SetTitleOffset(-999)
            h.GetYaxis().SetTitleOffset(1.7 * h.GetYaxis().GetTitleOffset())
            #r.SetOwnership(h, False)
            h.Sumw2()
            hr = r.TH1F("hr_%s_%s_%s" % (sample.name, ifile, varname), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
            #r.SetOwnership(hr, False)
            hr.Sumw2()

            weight_str = "w * %.2f * 1000. / %.2f" % (float(xsec), float(sumw))
            full_cut_str = "(%s) * %s" % (plot_selection, weight_str)

            cmd = "%s>>%s" % (varname, h.GetName())
            tree.Draw(cmd, full_cut_str, "goff")
            cmd = "%s>>%s" % (varname, hr.GetName())
            tree.Draw(cmd, full_cut_str, "goff")

            if h.Integral() == 0 : continue

            sample_histos[isample].Add(h)
            sample_ratio_histos[isample].Add(hr)

    # draw
    rc = RatioCanvas(name = "rc_%s" % varname)
    rc.canvas.cd()
    upper = rc.upper_pad
    lower = rc.lower_pad
    upper.SetTicks(1,1)
    upper.SetGrid(1,1)
    lower.SetTicks(1,1)
    lower.SetGrid(1,1)
    
    # upper pad
    for isample, sample in enumerate(samples) :
        sample_histos[isample].Scale(1.0 / sample_histos[isample].Integral())
        sample_histos[isample].SetLineColor(sample.color)
        sample_histos[isample].SetLineWidth(2)
    upper.cd()
    sample_histos[0].Draw("hist e")
    upper.Update()
    sample_histos[1].Draw("hist e same")
    upper.Update()

    leg = r.TLegend(0.72, 0.8, 0.92, 0.91)
    leg.AddEntry(sample_histos[0], "Sherpa 2.2.1", "l")
    leg.AddEntry(sample_histos[1], "MG5+aMC@NLO", "l")
    leg.Draw()
    upper.Update()

    # lower pad

    # save
    outname = "zhf_plots/zhf_plots_%s.pdf" % varname
    print("saving: %s" % (os.path.abspath(outname)))
    rc.canvas.SaveAs(outname)

def make_plots(samples, args) :

    var_dict = variables_dict()
    n_vars = len(var_dict)
    for ivar, varname in enumerate(var_dict.keys()) :
        print("[%02d/%02d] %s" % (ivar+1, n_vars, varname))
        make_plot(varname, samples, args)

def main() :

    parser = argparse.ArgumentParser(description = "Calculate Z+HF uncertainties")
    parser.add_argument("--xsec", action = "store_true", default = False,
        help = "Generate cross-section list"
    )
    parser.add_argument("--plots", action = "store_true", default = False,
        help = "Make plots too"
    )
    args = parser.parse_args()

    if args.xsec :
        gen_xsec_list(args)
        sys.exit()

    sample_nom = Sample("sherpa", "list_sherpa.txt", r.kBlack)
    sample_nom.load_xsec("xsec_file_%s.txt" % sample_nom.name)
    sample_alt = Sample("mg5", "list_mg5.txt", r.kRed)
    sample_alt.load_xsec("xsec_file_%s.txt" % sample_alt.name)
    samples = [sample_nom, sample_alt]

    
    if not args.plots :
        calc_uncerts(samples, args)
    elif args.plots :
        make_plots(samples, args)

if __name__ == "__main__" : 
    main()
