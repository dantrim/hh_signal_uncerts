#!/bin/env python

from __future__ import print_function

import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True
r.gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

import json
import sys, os
import argparse

class RatioCanvas :
    def __init__(self, name) :
        self.name = "c_" + name
        self.canvas = r.TCanvas(self.name,self.name, 768, 768)
        self.upper_pad = r.TPad("upper", "upper", 0.0, 0.0, 1.0, 1.0)
        self.lower_pad = r.TPad("lower", "lower", 0.0, 0.0, 1.0, 1.0)
        self.set_pad_dimensions()
    def set_pad_dimensions(self) :
        can = self.canvas
        up  = self.upper_pad
        dn  = self.lower_pad

        can.cd()
        up_height = 0.75
        dn_height = 0.30
        up.SetPad(0.0, 1.0-up_height, 1.0, 1.0)
        dn.SetPad(0.0, 0.0, 1.0, dn_height)

        up.SetTickx(0)
        dn.SetGrid(0)
        dn.SetTicky(0)

        up.SetFrameFillColor(0)
        up.SetFillColor(0)

        # set right margins
        up.SetRightMargin(0.05)
        dn.SetRightMargin(0.05)

        # set left margins
        up.SetLeftMargin(0.14)
        dn.SetLeftMargin(0.14)

        # set top margins
        up.SetTopMargin(0.7 * up.GetTopMargin())
        
        # set bottom margins
        up.SetBottomMargin(0.09)
        dn.SetBottomMargin(0.4)

        up.Draw()
        dn.Draw()
        can.Update()

        self.canvas = can
        self.upper_pad = up
        self.lower_pad = dn

def variables_dict() :

    v = {}
    v["NN_d_hh"] = [[20, 0, 10], "d_{hh}"]
    #v["NN_d_hh"] = [[20, -6, 10], "d_{hh}"]
    v["mbb"] = [[20, 20, 160], "m_{bb} [GeV]"]
    v["HT2Ratio"] = [[10, 0, 1], "H_{T2}^{R}"]
    v["dRll"] = [[10, 0, 4], "#Delta R_{ll}"]
    v["mt2_bb"] = [[10, 10, 150], "m_{t2}^{bb} [GeV]"]
    return v

def make_plot(varname, config, args) :

    input_file = config["input_file"]
    rfile = r.TFile.Open(input_file)
    sumw_hist = rfile.Get("h_sumw")
    tree = rfile.Get("truth")

    var_bounds = variables_dict()[varname]
    x_bounds = var_bounds[0]
    x_label = var_bounds[1]

    n_bins = x_bounds[0]
    x_lo = x_bounds[1]
    x_hi = x_bounds[2]

    nominal_weight = config["nominal"]
    weight_variations = config["variations"]

    all_weights = [nominal_weight]
    for wv in weight_variations :
        all_weights.append(wv)
    weight_idx_map = {}
    for iw, w in enumerate(all_weights) :
        weight_idx_map[w] = iw

    if args.scales and not args.pdf :
        tmp = []
        for wv in weight_variations :
            if "mu" in wv :
                tmp.append(wv)
        weight_variations = tmp

    if args.pdf and not args.scales :
        tmp = []
        for wv in weight_variations :
            if "PDF" in wv :
                tmp.append(wv)
        weight_variations = tmp

    if args.pdf and args.scales :
        weight_variations = weight_variations

    weights = [nominal_weight]
    for wv in weight_variations :
        weights.append(wv)

    tcut_str = "nBJets>=2 && mll>20 && mll<60"

    histos = []
    sumw_vals = []
    for iw, w in enumerate(weights) :
        h = r.TH1F("h_%s_%s" % (varname, w), ";%s;a.u." % x_label, n_bins, x_lo, x_hi)
        w_idx = weight_idx_map[w]
        sumw = sumw_hist.GetBinContent(w_idx + 1)
        sumw_vals.append(sumw)
        w_str = "hh_weights[%d]" % w_idx
        full_cut = "(%s) * %s" % (tcut_str, w_str)
        cmd = "%s>>%s" % (varname, h.GetName())
        tree.Draw(cmd, full_cut, "goff")
        err = r.Double(0.0)
        integral = h.IntegralAndError(0,-1, err)
        print("{} : {} +/- {}".format(w, integral, err))
        #h.Scale(1.0/h.Integral())
        #h.Scale(1.0 / sumw)
        histos.append(h)

    acceptance_histos = []
    acceptance_histos_ratio = []
    n_bins = histos[0].GetNbinsX()
    for ih, h in enumerate(histos) :
        h_acc = r.TH1F("h_acc_%s" % h.GetName(), ";%s;Acceptance" % x_label,  n_bins, x_lo, x_hi)
        h_acc.SetLineColor(r.kRed+3)
        h_acc.SetLineWidth(1)
        h_acc_r = r.TH1F("h_acc_%s" % h.GetName(), ";%s;Acceptance" % x_label,  n_bins, x_lo, x_hi)
        h_acc_r.SetLineColor(r.kBlack)
        h_acc_r.SetLineWidth(2)
        for ibin in range(n_bins) :
            bin_low = ibin+1
            integral = h.Integral(bin_low, -1)
            acceptance = integral / sumw_vals[ih]
            #acceptance_vals.append(acceptance)
            h_acc.SetBinContent(bin_low, 1.0 * acceptance)
            h_acc_r.SetBinContent(bin_low, 1.0 * acceptance)

        h_acc.GetXaxis().SetLabelOffset(-999)
        h_acc.GetXaxis().SetTitleOffset(-999)
        h_acc.GetYaxis().SetTitleOffset(1.7 * h.GetYaxis().GetTitleOffset())
        acceptance_histos.append(h_acc)
        acceptance_histos_ratio.append(h_acc_r)

    rc = RatioCanvas(name = "rc_%s" % varname)
    rc.canvas.cd()
    upper = rc.upper_pad
    lower = rc.lower_pad
    upper.SetTicks(1,1)
    upper.SetGrid(1,1)
    lower.SetTicks(1,1)
    lower.SetGrid(1,1)

    # upper pad
    upper.cd()
    acceptance_histos[0].Draw("hist")
    upper.Update()
    for ih, h in enumerate(acceptance_histos) :
        if ih == 0 : continue
        h.Draw("hist same")
        upper.Update()

    # lower pad
    lower.cd()

    den_histo = acceptance_histos_ratio[0]
    h_ratios = []
    for ih, h in enumerate(acceptance_histos_ratio) :
        if ih == 0 : continue
        h.Divide(den_histo)
        h_ratios.append(h)

    maxy = 1.05
    miny = 0.95
    if not args.scales and args.pdf :
        maxy = 1.01
        miny = 0.99

    for ih, h in enumerate(h_ratios) :

        h.GetYaxis().SetTitle("Var. / Nominal")
        h.GetYaxis().SetNdivisions(5)

        h.GetXaxis().SetLabelSize(2.5 * h.GetXaxis().GetLabelSize())
        h.GetXaxis().SetTitleSize(3 * h.GetXaxis().GetTitleSize())

        h.GetYaxis().SetLabelSize(2.5 * h.GetYaxis().GetLabelSize())
        h.GetYaxis().SetTitleSize(2.2 * h.GetYaxis().GetTitleSize())
        h.GetYaxis().SetTitleOffset(0.8 * h.GetYaxis().GetTitleOffset())

        h.SetMaximum(maxy)
        h.SetMinimum(miny)
        cmd = "hist"
        if ih > 0 :
            cmd += " same"
        h.Draw(cmd)
        lower.Update()

    line = r.TLine(x_lo, 1.0, x_hi, 1.0)
    line.SetLineStyle(2)
    line.SetLineWidth(2)
    line.SetLineColor(r.kRed)
    line.Draw("same")
    

    # save
    outname = "uncert_plots/hh_uncert_%s_ACC" % varname
    if args.scales :
        outname += "_scales"
    if args.pdf :
        outname += "_pdf"
    outname += ".pdf"
    print("Saving plot: %s" % os.path.abspath(outname))
    rc.canvas.SaveAs(outname)

#    n_bins = histos[0].GetNbinsX()
#    maxima = []
#    minima = []
#    for ibin in range(n_bins) :
#        bin_no = ibin+1
#        maxy = -1e9
#        miny = 1e9
#        for ih, h in enumerate(histos) :
#            yval = h.GetBinContent(bin_no)
#            if yval > maxy : maxy = yval
#            if yval < miny : miny = yval
#        maxima.append(maxy)
#        minima.append(miny)
#
#    delta_nom_up = []
#    delta_nom_dn = []
#    integral_delta_up = []
#    for ibin in range(n_bins) :
#        bin_no = ibin+1
#        nom_val = histos[0].GetBinContent(bin_no)
#        max_val = maxima[ibin]
#        min_val = minima[ibin]
#
#        nom_val = histos[0].Integral(ibin+1,-1)
#        max_val = sum(maxima[ibin:-1])
#        min_val = sum(minima[ibin:-1])
#
#        if nom_val == 0 :
#            delta_up = 0
#            delta_dn = 0
#        else :
#            delta_up = abs(max_val - nom_val) / nom_val
#            delta_dn = abs(nom_val - min_val) / nom_val
#
#        delta_nom_up.append(delta_up)
#        delta_nom_dn.append(delta_dn)
#
#    h_up = r.TH1F("h_%s_envelope_up" % varname, ";%s;a.u." % (x_label), n_bins, x_lo, x_hi)
#    h_up.SetLineColor(r.kBlue)
#    h_up.SetLineWidth(2)
#    h_dn = r.TH1F("h_%s_envelope_down" % varname, ";%s;a.u." % (x_label), n_bins, x_lo, x_hi)
#    h_dn.SetLineColor(r.kRed)
#    h_dn.SetLineWidth(2)
#    for idx, max_val in enumerate(maxima) :
#        min_val = minima[idx]
#        h_up.SetBinContent(idx+1, max_val)
#        h_dn.SetBinContent(idx+1, min_val)
#
#    for h in [h_up, h_dn] :
#        h.GetXaxis().SetLabelOffset(-999)
#        h.GetXaxis().SetTitleOffset(-999)
#        h.GetYaxis().SetTitleOffset(1.7 * h.GetYaxis().GetTitleOffset())
#
#    rc = RatioCanvas(name = "rc_%s" % varname)
#    rc.canvas.cd()
#    rc.upper_pad.cd() 
#    upper = rc.upper_pad
#    upper.SetTicks(1,1)
#    upper.SetGrid(1,1)
#    lower = rc.lower_pad
#    lower.SetTicks(1,1)
#    lower.SetGrid(1,1)
#    lower.Update()
#
#    h_up.Draw("hist")
#    upper.Update()
#    h_dn.Draw("hist same")
#    upper.Update()
#
#    # lower pad
#    lower.cd()
#    h_delta_up = r.TH1F("h_delta_up_%s" % varname, ";%s;#Delta_{nom}" % (x_label), n_bins, x_lo, x_hi)
#    h_delta_up.SetLineColor(r.kBlue)
#    h_delta_up.SetLineWidth(2)
#    h_delta_dn = r.TH1F("h_delta_dn_%s" % varname, ";%s;#Delta_{nom}" % (x_label), n_bins, x_lo, x_hi)
#    h_delta_dn.SetLineColor(r.kRed)
#    h_delta_dn.SetLineWidth(2)
#
#    h_delta_up.GetYaxis().SetNdivisions(5)
#    h_delta_dn.GetYaxis().SetNdivisions(5)
#
#    for ibin in range(n_bins) :
#        up_val = 1.0 + 1.0 * delta_nom_up[ibin]
#        dn_val = 1.0 - 1.0 * delta_nom_dn[ibin]
#
#        h_delta_up.SetBinContent(ibin+1, up_val)
#        h_delta_dn.SetBinContent(ibin+1, dn_val)
#
#    #h_delta_up.SetMaximum(1.1)
#    #h_delta_up.SetMinimum(0.9)
#    #h_delta_dn.SetMaximum(1.1)
#    #h_delta_dn.SetMinimum(0.9)
#    max_y = 1.2
#    min_y = 0.8
#    h_delta_up.SetMaximum(max_y)
#    h_delta_up.SetMinimum(min_y)
#    h_delta_dn.SetMaximum(max_y)
#    h_delta_dn.SetMinimum(min_y)
#
#    for h in [h_delta_up, h_delta_dn] :
#        h.GetXaxis().SetLabelSize(2.5 * h.GetXaxis().GetLabelSize())
#        h.GetXaxis().SetTitleSize(3 * h.GetXaxis().GetTitleSize())
#
#        h.GetYaxis().SetLabelSize(2.5 * h.GetYaxis().GetLabelSize())
#        h.GetYaxis().SetTitleSize(3.2 * h.GetYaxis().GetTitleSize())
#        h.GetYaxis().SetTitleOffset(0.5 * h.GetYaxis().GetTitleOffset())
#
#    h_delta_up.Draw("hist")
#    h_delta_dn.Draw("hist same")
#    lower.Update()
#
#    rc.canvas.Update()
#
#    outname = "uncert_plots/hh_uncert_%s_ACC" % varname
#    if args.scales :
#        outname += "_scales"
#    if args.pdf :
#        outname += "_pdf"
#    outname += ".pdf"
#    print("Saving plot: %s" % os.path.abspath(outname))
#    rc.canvas.SaveAs(outname)
            
def make_plots(config, args) :

    var_dict = variables_dict()
    if args.var :
        tmp = {}
        for v in var_dict :
            if v in args.var :
                tmp[v] = var_dict[v]
        var_dict = tmp
    n_vars = len(var_dict)
    for iv, v in enumerate(var_dict) :
        print("[%02d/%02d] plotting %s" % (iv+1, n_vars, v))
        make_plot(v, config, args)


def main() :

    parser = argparse.ArgumentParser(description = "Make some plots of the variatiosn")
    parser.add_argument("-i", "--input", required = True,
        help = "Provide the input JSON descriptor"
    )
    parser.add_argument("--scales", action = "store_true", default = False,
        help = "Make plots of only scale variations"
    )
    parser.add_argument("--pdf", action = "store_true", default = False,
        help = "Make plots of only PDF variations"
    )
    parser.add_argument("-v", "--var", nargs = "+",
        help = "Plot this set of variables"
    )
    args = parser.parse_args()

    if not os.path.isfile(args.input) :
        print("ERROR could not find input JSON file")
        sys.exit()

    if (not args.scales) and (not args.pdf) :
        print("ERROR You must provide scales or pdf option")
        sys.exit()

    with open(args.input) as input_file :
        data = json.load(input_file)
    input_file = data["input_file"]
    if not os.path.isfile(input_file) :
        print("ERROR could not find input ntuple file (={})".format(input_file))
        sys.exit()

    make_plots(data, args)

if __name__ == "__main__" :
    main()
