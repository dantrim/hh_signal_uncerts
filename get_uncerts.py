#!/bin/env python

from __future__ import print_function
import ROOT as r
r.PyConfig.IgnoreCommandLineOptions = True
r.gROOT.SetBatch(True)

import json
import sys, os
import argparse

def get_acceptance(region_cut, config, weight_idx, args) :

    filename = config["input_file"]
    rfile = r.TFile.Open(filename)
    sumw_hist = rfile.Get("h_sumw")
    sumw = sumw_hist.GetBinContent(weight_idx+1)

    tree = rfile.Get("truth")

    h = r.TH1F("h", "", 100, 0, -1)

    cmd = "NN_d_hh>>%s" % h.GetName()
    cut = "(%s) * hh_weights[%d]" % (region_cut, weight_idx)
    tree.Draw(cmd, cut, "goff")

    err = r.Double(0.0)
    integral = h.IntegralAndError(0, -1, err)

    acceptance = integral / sumw
    err /= sumw

    #return [integral, err]
    return [acceptance, err]

def get_uncert(region_name, region_cut, config, args) :

    print(55 * "=")

    nominal_weight =  config["nominal"]
    weight_variations = config["variations"]

    all_weights = [nominal_weight]
    for wv in weight_variations :
        all_weights.append(wv)
    weight_idx_map = {}
    for iw, w in enumerate(all_weights) :
        weight_idx_map[w] = iw

    scale_variation_weights = []
    pdf_variation_weights = []

    for wv in weight_variations :
        if wv == nominal_weight : continue
        if "mu" in wv :
            scale_variation_weights.append(wv)
        elif "PDF" in wv :
           pdf_variation_weights.append(wv)

    nom_val, nom_err = get_acceptance(region_cut, config, 0, args)
    #print("Nominal: %.4f +/- %.4f" % (nom_val, nom_err))

    scale_deltas = []
    #for scale_var in pdf_variation_weights :
    for scale_var in scale_variation_weights :
        val, err = get_acceptance(region_cut, config, weight_idx_map[scale_var], args)
        #print("%s: %.3f +/- %.3f" % (scale_var, val, err))
        if val > nom_val :
            scale_deltas.append(abs(val - nom_val) / float(nom_val))
        elif val < nom_val :
            scale_deltas.append(-1.0 * abs(nom_val - val) / float(nom_val))
    max_scale_delta = max(scale_deltas)
    min_scale_delta = min(scale_deltas)
    print(55 * "-")
    print("{} - Max scale delta         : {}".format(region_name, max_scale_delta))
    print("{} - Min scale delta         : {}".format(region_name, min_scale_delta))
    print(23 * "- ")
    sym_scale_delta = 0.5 * (abs(max_scale_delta) + abs(min_scale_delta))
    print("{} - Symmetrized SCALE delta : {}".format(region_name, sym_scale_delta))

    pdf_deltas = []
    for pdf_var in pdf_variation_weights :
        val, err = get_acceptance(region_cut, config, weight_idx_map[pdf_var], args)
        if val > nom_val :
            pdf_deltas.append(abs(val - nom_val) / float(nom_val))
        elif val < nom_val :
            pdf_deltas.append(-1.0 * abs(nom_val - val) / float(nom_val))

    max_pdf_delta = max(pdf_deltas)
    min_pdf_delta = min(pdf_deltas)
    print(55 * "-")
    print("%s - Max PDF delta          : %.4f" % (region_name, max_pdf_delta))
    print("%s - Min PDF delta          : %.4f" % (region_name, min_pdf_delta))
    print(23 * "- ")
    sym_pdf_delta = 0.5 * (abs(max_pdf_delta) + abs(min_pdf_delta))
    print("%s - Symmetrized PDF delta  : %.4f" % (region_name, sym_pdf_delta))

def get_uncerts(data, args) :

    regions = data["regions"]
    for region in regions :
        rname = region["name"]
        cut = region["cut"]
        get_uncert(rname, cut, data, args)

def main() :

    parser = argparse.ArgumentParser(description = "Get uncertainties on hh")
    parser.add_argument("-i", "--input", required = True,
        help = "Provide the input JSON descriptor"
    )
    args = parser.parse_args()

    if not os.path.isfile(args.input) :
        print("ERROR could not find input JSON file")
        sys.exit()

    with open(args.input) as input_file :
        data = json.load(input_file)
    input_file = data["input_file"]
    if not os.path.isfile(input_file) :
        print("ERROR could not find input ntuple file (={})".format(input_file))
        sys.exit()

    get_uncerts(data, args)

if __name__ == "__main__" :
    main()
